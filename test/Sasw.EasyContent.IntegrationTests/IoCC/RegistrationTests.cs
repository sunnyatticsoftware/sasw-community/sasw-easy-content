﻿using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Sasw.EasyContent.Contracts.Services;
using Sasw.EasyContent.IoCC;
using Sasw.EasyContent.IoCC.Options;
using Sasw.EasyContent.Services;
using Sasw.TestSupport.XUnit;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Sasw.EasyContent.IntegrationTests.IoCC
{
    public static class RegistrationTests
    {
        public class Given_A_Dependency_Injection_Container_With_EasyContent_Registered_When_Resolving_IBlogQueryService
            : Given_When_Then_Test_Async
        {
            private IServiceProvider _sut;
            private IBlogQueryService _result;

            protected override Task Given()
            {
                _sut =
                    new ServiceCollection()
                        .AddEasyContent(
                            sp => PostOptions.Default,
                            sp => new FileProviderOptions())
                        .BuildServiceProvider();
                
                return Task.CompletedTask;
            }

            protected override Task When()
            {
                _result = _sut.GetService<IBlogQueryService>();
                return Task.CompletedTask;
            }

            [Fact]
            public void Then_It_Should_Be_A_BlogQueryService()
            {
                _result.Should().BeAssignableTo<BlogQueryService>();
            }
        }
    }
}