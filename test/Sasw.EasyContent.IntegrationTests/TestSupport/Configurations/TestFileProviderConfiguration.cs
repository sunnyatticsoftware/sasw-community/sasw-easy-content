﻿using Microsoft.Extensions.FileProviders;
using Sasw.EasyContent.Contracts.Configurations;
using System;

namespace Sasw.EasyContent.IntegrationTests.TestSupport.Configurations
{
    public class TestFileProviderConfiguration
        : IFileProviderConfiguration
    {
        public IFileProvider FileProvider { get; set; } = new PhysicalFileProvider(Environment.CurrentDirectory);
    }
}
