﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Sasw.EasyContent.IntegrationTests.TestSupport.IoCC
{
    public static class RegistrationExtensions
    {
        public static IServiceCollection OverrideWith(this IServiceCollection serviceCollection, Func<IServiceCollection, IServiceCollection> overrides)
        {
            var result = overrides.Invoke(serviceCollection);
            return result;
        }
    }
}
