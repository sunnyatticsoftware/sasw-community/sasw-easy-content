﻿using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Sasw.EasyContent.Builders;
using Sasw.EasyContent.Contracts.Configurations;
using Sasw.EasyContent.Contracts.Models;
using Sasw.EasyContent.Contracts.Services;
using Sasw.EasyContent.IntegrationTests.TestSupport.Configurations;
using Sasw.EasyContent.IntegrationTests.TestSupport.IoCC;
using Sasw.EasyContent.IoCC;
using Sasw.EasyContent.IoCC.Options;
using Sasw.TestSupport;
using Sasw.TestSupport.XUnit;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Sasw.EasyContent.IntegrationTests.Services.BlogQueryServiceTests
{
    public static class GetPostSummariesTests
    {
        public class Given_Three_Posts_Available_And_No_Filter_When_Getting_Summary_Posts
            : Given_When_Then_Test_Async
        {
            private IBlogQueryService _sut;
            private IEnumerable<IPostSummary> _result;
            private IPostFilter _postFilter;

            protected override Task Given()
            {
                var serviceProvider =
                    new ServiceCollection()
                        .AddEasyContent(
                            sp => new PostOptions(),
                            sp => new FileProviderOptions())
                        .OverrideWith(
                            serviceCollection =>
                            {
                                serviceCollection.Replace(ServiceDescriptor.Singleton(typeof(IFileProviderConfiguration), typeof(TestFileProviderConfiguration)));
                                serviceCollection.Replace(ServiceDescriptor.Singleton(typeof(IPostConfiguration), typeof(TestPostConfiguration)));
                                return serviceCollection;
                            })
                        .BuildServiceProvider();

                _postFilter =
                    new PostFilterBuilder()
                        .Build();

                _sut = serviceProvider.GetService<IBlogQueryService>();

                return Task.CompletedTask;
            }

            protected override async Task When()
            {
                _result = await _sut.GetPostSummaries(_postFilter);
            }

            [Fact]
            public void Then_It_Should_Return_Three_Post_Summaries()
            {
                _result.Should().HaveCount(3);
            }
        }
    }
}