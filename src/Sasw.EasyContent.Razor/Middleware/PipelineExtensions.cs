﻿using Microsoft.AspNetCore.Builder;

namespace Sasw.EasyContent.Razor.Middleware
{
    public static class PipelineExtensions
    {
        public static IApplicationBuilder UseEasyContent(this IApplicationBuilder builder)
        {
            builder.UseMiddleware<PostProcessor>();
            return builder;
        }
    }
}
