﻿using Microsoft.AspNetCore.Http;
using Sasw.EasyContent.Configurations;
using Sasw.EasyContent.Contracts.Configurations;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Sasw.EasyContent.Razor.Middleware
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class PostProcessor
    {
        private readonly RequestDelegate _next;
        private readonly IPostConfiguration _postConfiguration;

        public PostProcessor(
            RequestDelegate next,
            IPostConfiguration postConfiguration)
        {
            _next = next;
            _postConfiguration = postConfiguration;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var postsRootFolder = $"{_postConfiguration.PostRootFolder.TrimEnd('/')}/";
            var requestedPath = context.Request.Path.Value;
            var hasExtension = Path.HasExtension(requestedPath);
            var isAsset = requestedPath != null && hasExtension && !requestedPath.EndsWith(_postConfiguration.PostExtension);
            var isPost = requestedPath != null && requestedPath.Contains(postsRootFolder, StringComparison.InvariantCultureIgnoreCase) && !isAsset;
            if (!isPost)
            {
                await _next(context);
                return;
            }

            context.Items[Constants.RequestedPathParamsKey] = requestedPath;
            var postViewerRoute = _postConfiguration.PostViewerRoute;
            context.Request.Path = postViewerRoute;

            await _next(context);
        }
    }
}
