﻿using System.Collections.Generic;

namespace Sasw.EasyContent.Razor.Pages
{
    using Configurations;
    using Contracts.Services;
    using Microsoft.AspNetCore.Html;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    using System;
    using System.Threading.Tasks;

    public class PostModel
        : PageModel
    {
        private readonly IBlogQueryService _blogQueryService;
        public string Title { get; private set; }
        public string Author { get; private set; }
        public DateTime PublishedOn { get; private set; }
        public HtmlString HtmlContent { get; private set; }
        public string Image { get; private set; }
        public string LanguageCode { get; private set; }
        public IEnumerable<string> Tags { get; private set; }
        public string RelativePath { get; private set; }

        public PostModel(IBlogQueryService blogQueryService)
        {
            _blogQueryService = blogQueryService;
        }

        public async Task OnGetAsync()
        {
            var relativePath = HttpContext.Items[Constants.RequestedPathParamsKey] as string;
            var post = await _blogQueryService.GetPost(relativePath);
            Title = post.Title;
            Author = post.Author;
            PublishedOn = post.PublishedOn;
            HtmlContent = new HtmlString(post.Content);
            Image = post.Image;
            LanguageCode = post.LanguageCode;
            Tags = post.Tags;
            RelativePath = post.RelativePath;
        }
    }
}
