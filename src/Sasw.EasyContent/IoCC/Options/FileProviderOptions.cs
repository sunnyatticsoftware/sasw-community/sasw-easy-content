﻿using Microsoft.Extensions.FileProviders;

namespace Sasw.EasyContent.IoCC.Options
{
    public class FileProviderOptions
    {
        public IFileProvider FileProvider { get; set; }
    }
}
