﻿using System;

namespace Sasw.EasyContent.Exceptions
{
    public class ParsingException
        : Exception
    {
        public ParsingException(string message)
            : base(message)
        {
        }
    }
}
