﻿using Microsoft.Extensions.FileProviders;
using Sasw.EasyContent.Contracts.Configurations;


namespace Sasw.EasyContent.Configurations
{
    internal sealed class FileProviderConfiguration
        : IFileProviderConfiguration
    {
        public IFileProvider FileProvider { get; }

        public FileProviderConfiguration(IFileProvider fileProvider)
        {
            FileProvider = fileProvider;
        }
    }
}
