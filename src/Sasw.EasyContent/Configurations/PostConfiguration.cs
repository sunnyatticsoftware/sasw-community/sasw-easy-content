﻿using Sasw.EasyContent.Contracts.Configurations;
using System;

namespace Sasw.EasyContent.Configurations
{
    internal sealed class PostConfiguration
        : IPostConfiguration
    {
        public string PostRootFolder { get; }
        public string PostExtension { get; }
        public string PostViewerRoute { get; }

        public PostConfiguration(string postsRootFolder, string postsExtension, string postViewerRoute)
        {
            PostRootFolder = postsRootFolder ?? throw new ArgumentNullException(nameof(postsRootFolder));
            PostExtension = postsExtension ?? throw new ArgumentNullException(nameof(postsExtension));
            PostViewerRoute = postViewerRoute ?? throw new ArgumentNullException(nameof(postViewerRoute));
        }
    }
}
