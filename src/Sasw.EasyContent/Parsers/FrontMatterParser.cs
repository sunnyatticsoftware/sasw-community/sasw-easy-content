﻿using Sasw.EasyContent.Contracts.Models;
using Sasw.EasyContent.Contracts.Parsers;
using Sasw.EasyContent.Exceptions;
using Sasw.EasyContent.Models;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace Sasw.EasyContent.Parsers
{
    public class FrontMatterParser
        : IFrontMatterParser
    {
        public IPostMetadata Parse(string frontMatter)
        {
            var yamlDeserializer =
                new DeserializerBuilder()
                    .WithNamingConvention(new CamelCaseNamingConvention())
                    .IgnoreUnmatchedProperties()
                    .Build();

            var post = yamlDeserializer.Deserialize<PostMetadata>(frontMatter);

            if (post is null)
            {
                throw new ParsingException($"Could not deserialize front matter {frontMatter}. Posts require metadata surrounded by '---'.");
            }

            return post;
        }
    }
}
