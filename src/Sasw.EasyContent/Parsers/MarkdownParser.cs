﻿using Markdig;
using Sasw.EasyContent.Contracts.Parsers;
using System;

namespace Sasw.EasyContent.Parsers
{
    public class MarkdownParser
        : IMarkdownParser
    {
        public string Parse(string markdown)
        {
            if (markdown is null)
            {
                throw new ArgumentNullException(nameof(markdown));
            }
            var pipeline =
                new MarkdownPipelineBuilder()
                    .UseAdvancedExtensions()
                    .UseYamlFrontMatter()
                    .Build();

            var html = Markdown.ToHtml(markdown, pipeline).Trim();
            return html;
        }
    }
}
