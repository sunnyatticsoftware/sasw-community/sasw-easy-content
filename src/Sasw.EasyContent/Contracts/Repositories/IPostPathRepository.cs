﻿using System.Collections.Generic;

namespace Sasw.EasyContent.Contracts.Repositories
{
    public interface IPostPathRepository
    {
        IEnumerable<string> GetAllPostFullPaths();
        string GetPostFullPath(string relativeLink);
        string GetPostRelativePath(string fullPath);
    }
}