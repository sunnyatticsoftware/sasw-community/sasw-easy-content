﻿using Sasw.EasyContent.Contracts.Models;
using System.Threading.Tasks;

namespace Sasw.EasyContent.Contracts.Repositories
{
    public interface IPostRepository
    {
        Task<IPost> GetPost(string relativePath);
    }
}