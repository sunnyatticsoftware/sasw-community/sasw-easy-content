﻿using Sasw.EasyContent.Contracts.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sasw.EasyContent.Contracts.Repositories
{
    public interface IPostSummaryRepository
    {
        Task<IEnumerable<IPostSummary>> GetPostSummaries();
    }
}