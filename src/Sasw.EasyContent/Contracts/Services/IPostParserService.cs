﻿using Sasw.EasyContent.Contracts.Models;

namespace Sasw.EasyContent.Contracts.Services
{
    public interface IPostParserService
    {
        IPostMetadata GetPostMetadata(string content);
        IPost GetPost(string content, string relativePath);
    }
}