﻿using Sasw.EasyContent.Contracts.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sasw.EasyContent.Contracts.Services
{
    public interface IBlogQueryService
    {
        Task<IEnumerable<IPostSummary>> GetPostSummaries(IPostFilter postFilter);
        Task<IPost> GetPost(string relativePath);
    }
}