﻿using Sasw.EasyContent.Contracts.Models;

namespace Sasw.EasyContent.Contracts.Parsers
{
    public interface IFrontMatterParser
    {
        IPostMetadata Parse(string frontMatter);
    }
}