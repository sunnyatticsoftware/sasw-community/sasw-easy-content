﻿using Microsoft.Extensions.FileProviders;

namespace Sasw.EasyContent.Contracts.Configurations
{
    public interface IFileProviderConfiguration
    {
        IFileProvider FileProvider { get; }
    }
}