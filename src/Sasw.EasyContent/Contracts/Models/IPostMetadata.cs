﻿using System;
using System.Collections.Generic;

namespace Sasw.EasyContent.Contracts.Models
{
    public interface IPostMetadata
    {
        string Title { get; }
        string Summary { get; }
        string Author { get; }
        string Image { get; }
        string LanguageCode { get; }
        DateTime PublishedOn { get; }
        IEnumerable<string> Tags { get; }
        bool IsDraft { get; }
    }
}